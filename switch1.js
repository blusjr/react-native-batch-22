var tanggal = 12; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 4; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var total = "Tanggal Sekarang:" + tanggal +" " +  bulan + "" + tahun;
//Misal tanggal yang diberikan adalah hari 1, bulan 5, dan tahun 1945. Maka, output yang harus kamu proses adalah menjadi 1 Mei 1945.

switch(total) {
  case 1:   { console.log('12 April 1945'); break; }
  case 2:   { console.log('Bukan 12 April 1945!'); break; }
  case 3:   { console.log('12 April 1955'); break; }
  case 4:   { console.log('Loh'); break; }
  default:  { console.log('Tidak terjadi apa-apa'); }}